%define first_el 0

%macro colon 2
	%ifstr %1
		%ifid %2
			%%new_el: 
				dq first_el
				db %1, 0
			%2:
				%define first_el %%new_el
		%else
			%error "Second arg must be id"
		%endif
	%else
		%error "First arg must be string"
	%endif
%endmacro
