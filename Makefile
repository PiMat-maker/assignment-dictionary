ASM        = nasm
ASMFLAGS   =-felf64 -g

ex: dict.o lib.o main.o
	ld -o $@ $^

%.o: %.asm *.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	rm -f *.o ex
