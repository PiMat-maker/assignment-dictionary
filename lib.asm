section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .count:
    mov rdx, [rdi+rax]
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .count
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, 0xA
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rsi, rsp
    sub rsp, 21
    mov [rsi], byte 0
    dec rsi
    .loop:
	mov rax, rdi
	mov rdx, 0
        mov rbx, 10
	div rbx
	cmp rax, 0
    	je .print
	mov rdi, rax
	mov rax, rdx
	add rax, 48
        mov byte [rsi], al
	sub rax, 48
        dec rsi
	jmp .loop
    .print:
    mov rdi, rax
    mov rax, rdx
    add rax, 48
    mov byte [rsi], al
    sub rax, 48
    mov rdi, rsi
    call print_string
    add rsp, 21
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .pos
    neg rdi
    push rdi
    mov rdi, 45
    call print_char
    pop rdi
    .pos:
    	push rdi
    	call print_uint
        pop rdi
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 0
    mov r8, 0
        .loop:
	mov rax, [rsi+r8]
        cmp byte [rdi+r8], al
        jne .zero
	cmp byte [rdi+r8], 0
        je .one
        inc r8
        jmp .loop
    .one:
    mov rax, 1
    jmp .end
    .zero:
    mov rax, 0
    .end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 8
    mov r8, rsp
    mov rax, 0
    mov rdi, 0
    mov rsi, r8
    mov rdx, 1
    syscall
    cmp rax, 0
    je .end
    mov rax, [r8]
    .end:
    add rsp, 8
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rsi
        .space_loop:
        call read_char
        cmp byte rax, 0x20
        je .space_loop
        cmp byte rax, 0x9
        je .space_loop
        cmp byte rax, 0xA
        je .space_loop
    .pop_el:
    mov r8, 0
    pop rdx
    pop rsi
    push rsi
    cmp rax, 0 
    je .result
    cmp r8, rdx
    je .overflow
    mov byte [rsi], al
    inc r8
    .loop:
        push r8
	push rdx
	push rsi
	call read_char
	pop rsi
	pop rdx
	pop r8
	cmp rax, 0
	je .result
	cmp byte rax, 0x20
        je .result
        cmp byte rax, 0x9
        je .result
        cmp byte rax, 0xA
        je .result
	mov byte [rsi+r8], al
	inc r8
	cmp r8, rdx
	je .overflow
	jmp .loop 
    .overflow:
	pop rax
    	mov rax, 0
	mov rdx, 0
    	jmp .end
    .result:
	mov byte [rsi+r8], 0
	pop rax
	mov rdx, r8
    .end:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rdx, 0
    push rbx
    mov rax, 0
    .loop:
        cmp byte [rdi+rdx], 0
        je .end
        cmp byte [rdi+rdx], 48
        jb .end
        cmp byte [rdi+rdx], 57
        ja .end
	mov bl, [rdi+rdx]
	mov r8, rbx
	sub r8, 48
	mov rbx, 10
	push rdx
	mul rbx
	pop rdx
	add rax, r8
	inc rdx
	jmp .loop
    .end:  
    pop rbx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8, 1
    mov rdx, 0
    cmp byte [rdi], 0x2d
    jne .get_uint
    inc rdi
    inc rdx
    neg r8
    .get_uint:
	push r8
	push rdx
	call parse_uint
	pop r9
	pop r8
	cmp rdx, 0
	je .end
	add rdx, r9
	push rbx
	mov rbx, r8
	push rdx
	imul rbx
	pop rdx
	pop rbx
    .end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jae .zero
    mov rax, 0	
    .loop:
	mov r8, [rdi+rax]
        mov [rsi+rax], r8
        inc rax
        cmp byte [rdi+rax-1], 0
        je .end
        jmp .loop
    .zero:
    mov rax, 0
    .end:
    ret
