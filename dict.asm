section .text

extern string_equals
global find_word

find_word:
	mov rax, rsi
	.loop:
		test rax, rax
		jz .not_found
		push rax
		add rax, 8
		mov rsi, rax
		push rdi
		call string_equals
		pop rdi
		cmp rax, 1
		je .found
		pop rax
		mov rax, [rax]
		jmp .loop
	.not_found:
	mov rax, 0
	jmp .end
	.found:
	pop rax
	.end:
	ret
