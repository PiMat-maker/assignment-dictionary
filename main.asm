section .text

%define buf_size 256 

extern find_word
extern print_string
extern read_word
extern exit
extern string_length
extern print_newline

section .rodata
	%define stdout 1
	%define stderr 2
	%include "words.inc"
	not_found_el: db "The element not found", 0
	input_msg: db "Please, enter the desired element: ", 0
	wrong_input: db "Wrong input. Too long string", 0


section .text

global _start

_start:
	sub rsp, buf_size
	mov r8, rsp
	mov rdi, input_msg
	mov rsi, 1
	push r8
	call print_string
	pop rdi
	mov rsi, buf_size
	call read_word
	test rax, rax
	jz .input_error
	push rax
	mov rdi, rax
	mov rsi, first_el
	call find_word
	pop rdi
	test rax, rax
	jz .not_found
	push rax
	call string_length
	pop rdi
	add rax, 8
	inc rax
	add rdi, rax
	mov rsi, stdout
	call print_string
	mov rdi, 0
	jmp .end
.not_found:
	mov rdi, not_found_el
	mov rsi, stderr
	call print_string
	mov rdi, 1
	jmp .end
.input_error:
	mov rdi, wrong_input
	mov rsi, stderr
	call print_string
	mov rdi, 1
.end:
	push rdi
	call print_newline
	pop rdi
	call exit


